<?php

namespace Test\Helpers;

use Bnet\Cart\Cart;
use Mockery as m;
use Test\TestCase;

/**
 * User: thorsten
 * Date: 27.01.20
 * Time: 15:01
 */

class CartBase extends TestCase {

    /**
     * @var \Bnet\Cart\Cart
     */
    protected $cart;

    public function setUp(): void {
        $events = m::mock('\Illuminate\Contracts\Events\Dispatcher');
        $events->shouldReceive('dispatch');

        $this->cart = new Cart(
            new SessionMock(),
            $events,
            'shopping',
            'SAMPLESESSIONKEY'
        );
    }

    public function tearDown(): void {
        m::close();
    }
}