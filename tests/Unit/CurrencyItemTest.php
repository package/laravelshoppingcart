<?php

namespace Test\Unit;

use Bnet\Money\Money;
use Test\TestCase;
use Bnet\Cart\Condition;

/**
 * User: thorsten
 * Date: 22.07.16
 * Time: 13:50
 */
class CurrencyItemTest extends TestCase {

	public function testStaticFieldAccess() {
		$money = new Money(123456);
		$item = new \Bnet\Cart\CurrencyItem([
			'id' => 999,
			'name' => 'Sample Item 1',
			'price' => $money,
			'quantity' => 3,
			'blub' => 'x0',
			'attributes' => array(
				'test1' => 'Eins',
				'test2' => 'Zwei',
			)
		], 'EUR');

		$this->assertEquals('Sample Item 1', $item->name);
		$this->assertEquals(999, $item->id);
		$this->assertEquals(3, $item->quantity);
		$this->assertTrue($money->equals($item->price));
		$this->assertTrue($money->equals($item->price()));
		$this->assertEquals(123456, $item->price->amount());
		$this->assertEquals(3*123456, $item->priceSum()->amount());

		// undefined param
		$this->assertEquals('x0', $item->blub);
	}

	public function testTaxOnlyOnItem() {
		$money_with_tax = \Bnet\Money\MoneyGross::fromNet(1000, 20);
		$money_no_tax = new Money(1000);

		$single_item_with_tax = new \Bnet\Cart\CurrencyItem([
			'id' => 999,
			'name' => 'Item x',
			'price' => $money_with_tax,
			'quantity' => 1,
		], 'EUR');
		$multi_item_with_tax = new \Bnet\Cart\CurrencyItem([
			'id' => 999,
			'name' => 'Item x',
			'price' => $money_with_tax,
			'quantity' => 3,
		], 'EUR');

		$single_item_no_tax = new \Bnet\Cart\CurrencyItem([
			'id' => 999,
			'name' => 'Item x',
			'price' => $money_no_tax,
			'quantity' => 1,
		], 'EUR');

		$this->assertEquals(200, $single_item_with_tax->taxAmountWithConditions());
		$this->assertEquals(3*200, $multi_item_with_tax->taxAmountWithConditions());
		$this->assertEquals(0, $single_item_no_tax->taxAmountWithConditions());
	}

	public function testTaxOnlyOnItemAttribute() {
		$money_with_tax = \Bnet\Money\MoneyGross::fromNet(1000, 20);
		$money_no_tax = new Money(1000);

		$condition_percent_plus = new \Bnet\Cart\CurrencyCondition([
			'name' => 'Percent Plus',
			'type' => 'misc2',
			'target' => Condition::TARGET_ITEM,
			'value' => '+10%',
		]);
		$condition_percent_minus = new \Bnet\Cart\CurrencyCondition([
			'name' => 'Percent Minus',
			'type' => 'misc2',
			'target' => Condition::TARGET_ITEM,
			'value' => '-10%',
		]);
		$condition_with_tax_plus = new \Bnet\Cart\CurrencyCondition([
			'name' => 'Absolute Tax Plus',
			'type' => 'misc2',
			'target' => Condition::TARGET_ITEM,
			'value' => new \Bnet\Money\TaxedMoney(500, 'EUR', 20),
		]);
		$condition_with_tax_minus = new \Bnet\Cart\CurrencyCondition([
			'name' => 'Absolute Tax Minus',
			'type' => 'misc2',
			'target' => Condition::TARGET_ITEM,
			'value' => new \Bnet\Money\TaxedMoney(-500, 'EUR', 20),
		]);
		$condition_no_tax_plus = new \Bnet\Cart\CurrencyCondition([
			'name' => 'Absolute Plus',
			'type' => 'misc2',
			'target' => Condition::TARGET_ITEM,
			'value' => new Money(500, 'EUR'),
		]);
		$condition_no_tax_minus = new \Bnet\Cart\CurrencyCondition([
			'name' => 'Absolute Minus',
			'type' => 'misc2',
			'target' => Condition::TARGET_ITEM,
			'value' => new Money(-500, 'EUR'),
		]);

		/* here come the tests */

		$single_item_with_tax_percent_plus = new \Bnet\Cart\CurrencyItem([
			'id' => 999,
			'name' => 'Item x',
			'price' => $money_with_tax,
			'quantity' => 1,
			'conditions' => [$condition_percent_plus],
		], 'EUR');
		$this->assertEquals(220, $single_item_with_tax_percent_plus->taxAmountWithConditions());

		$single_item_with_tax_percent_minus = new \Bnet\Cart\CurrencyItem([
			'id' => 999,
			'name' => 'Item x',
			'price' => $money_with_tax,
			'quantity' => 1,
			'conditions' => [$condition_percent_minus],
		], 'EUR');
		$this->assertEquals(180, $single_item_with_tax_percent_minus->taxAmountWithConditions());

		$single_item_no_tax_percent_plus = new \Bnet\Cart\CurrencyItem([
			'id' => 999,
			'name' => 'Item x',
			'price' => $money_no_tax,
			'quantity' => 1,
			'conditions' => [$condition_percent_plus],
		], 'EUR');
		$this->assertEquals(0, $single_item_no_tax_percent_plus->taxAmountWithConditions());

		$single_item_with_tax_plus_tax = new \Bnet\Cart\CurrencyItem([
			'id' => 999,
			'name' => 'Item x',
			'price' => $money_with_tax,
			'quantity' => 1,
			'conditions' => [$condition_with_tax_plus],
		], 'EUR');
		$this->assertEquals(300, $single_item_with_tax_plus_tax->taxAmountWithConditions());

		$single_item_with_tax_minus_tax = new \Bnet\Cart\CurrencyItem([
			'id' => 999,
			'name' => 'Item x',
			'price' => $money_with_tax,
			'quantity' => 1,
			'conditions' => [$condition_with_tax_minus],
		], 'EUR');
		$this->assertEquals(100, $single_item_with_tax_minus_tax->taxAmountWithConditions());

		$single_item_with_tax_plus_no_tax = new \Bnet\Cart\CurrencyItem([
			'id' => 999,
			'name' => 'Item x',
			'price' => $money_with_tax,
			'quantity' => 1,
			'conditions' => [$condition_no_tax_plus],
		], 'EUR');
		$this->assertEquals(200, $single_item_with_tax_plus_no_tax->taxAmountWithConditions());

		$single_item_with_tax_minus_no_tax = new \Bnet\Cart\CurrencyItem([
			'id' => 999,
			'name' => 'Item x',
			'price' => $money_with_tax,
			'quantity' => 1,
			'conditions' => [$condition_no_tax_minus],
		], 'EUR');
		$this->assertEquals(200, $single_item_with_tax_minus_no_tax->taxAmountWithConditions());

		$single_item_no_tax_plus_tax = new \Bnet\Cart\CurrencyItem([
			'id' => 999,
			'name' => 'Item x',
			'price' => $money_no_tax,
			'quantity' => 1,
			'conditions' => [$condition_with_tax_plus],
		], 'EUR');
		$this->assertEquals(100, $single_item_no_tax_plus_tax->taxAmountWithConditions());

		$single_item_no_tax_minus_tax = new \Bnet\Cart\CurrencyItem([
			'id' => 999,
			'name' => 'Item x',
			'price' => $money_no_tax,
			'quantity' => 1,
			'conditions' => [$condition_with_tax_minus],
		], 'EUR');
		$this->assertEquals(0, $single_item_no_tax_minus_tax->taxAmountWithConditions());

		$single_item_no_tax_plus_no_tax = new \Bnet\Cart\CurrencyItem([
			'id' => 999,
			'name' => 'Item x',
			'price' => $money_no_tax,
			'quantity' => 1,
			'conditions' => [$condition_no_tax_plus],
		], 'EUR');
		$this->assertEquals(0, $single_item_no_tax_plus_no_tax->taxAmountWithConditions());

		$single_item_no_tax_minus_no_tax = new \Bnet\Cart\CurrencyItem([
			'id' => 999,
			'name' => 'Item x',
			'price' => $money_no_tax,
			'quantity' => 1,
			'conditions' => [$condition_no_tax_minus],
		], 'EUR');
		$this->assertEquals(0, $single_item_no_tax_minus_no_tax->taxAmountWithConditions());
	}
}
