<?php
/**
 * Created by PhpStorm.
 * User: darryl
 * Date: 1/16/2015
 * Time: 1:45 PM
 */

namespace Test\Unit;

use Bnet\Cart\Cart;
use Mockery as m;
use Test\Helpers\CartBase;
use Test\Helpers\SessionMock;

class CartTestMultipleInstances extends CartBase {

	/**
	 * @var Bnet\Cart\Cart
	 */
	protected $cart2;

	public function setUp(): void {
		$events = m::mock('Illuminate\Contracts\Events\Dispatcher');
		$events->shouldReceive('dispatch');

		$this->cart2 = new Cart(
			new SessionMock(),
			$events,
			'wishlist',
			'uniquesessionkey456'
		);

		parent::setUp();
	}

	public function test_cart_multiple_instances() {
		// add 3 items on cart 1
		$itemsForCart1 = array(
			array(
				'id' => 456,
				'name' => 'Sample Item 1',
				'price' => 6799,
				'quantity' => 4,
				'attributes' => array()
			),
			array(
				'id' => 568,
				'name' => 'Sample Item 2',
				'price' => 6925,
				'quantity' => 4,
				'attributes' => array()
			),
			array(
				'id' => 856,
				'name' => 'Sample Item 3',
				'price' => 5025,
				'quantity' => 4,
				'attributes' => array()
			),
		);

		$this->cart->add($itemsForCart1);

		$this->assertFalse($this->cart->isEmpty(), 'Cart should not be empty');
		$this->assertEquals(3, count($this->cart->items()->toArray()), 'Cart should have 3 items');
		$this->assertEquals('shopping', $this->cart->getInstanceName(), 'Cart 1 should have instance name of "shopping"');

		// add 1 item on cart 2
		$itemsForCart2 = array(
			array(
				'id' => 456,
				'name' => 'Sample Item 1',
				'price' => 6799,
				'quantity' => 4,
				'attributes' => array()
			),
		);

		$this->cart2->add($itemsForCart2);

		$this->assertFalse($this->cart2->isEmpty(), 'Cart should not be empty');
		$this->assertEquals(1, count($this->cart2->items()->toArray()), 'Cart should have 3 items');
		$this->assertEquals('wishlist', $this->cart2->getInstanceName(), 'Cart 2 should have instance name of "wishlist"');
	}
}