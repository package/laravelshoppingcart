<?php
/**
 * Created by PhpStorm.
 * User: darryl
 * Date: 3/18/2015
 * Time: 6:17 PM
 */

namespace Test\Unit;

use Test\Helpers\CartBase;

class ItemTest extends CartBase {

	public function test_item_get_sum_price_using_property() {
		$this->cart->add(455, 'Sample Item', 10099, 2, array());

		$item = $this->cart->get(455);

		$this->assertEquals(455, $item->id, 'Item summed price should be 20198');
		$this->assertEquals(20198, $item->priceSum(), 'Item summed price should be 20198');
	}

	public function test_item_get_sum_price_using_array_style() {
		$this->cart->add(455, 'Sample Item', 10099, 2, array());

		$item = $this->cart->get(455);

		$this->assertEquals(20198, $item->priceSum(), 'Item summed price should be 20198');
	}
}