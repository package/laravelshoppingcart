<?php
/**
 * User: thorsten
 * Date: 13.07.16
 * Time: 23:20
 */

namespace Bnet\Cart;


use Bnet\Money\Currency;
use Bnet\Money\Money;
use Bnet\Money\TaxedMoney;

/**
 * Class CurrencyItem
 * @package Bnet\Cart
 * @inheritdoc
 * @property Money|null price
 */
class CurrencyItem extends Item {

	/**
	 * @var Currency|string the currency for this Item for generating Sum Money Objects
	 */
	protected $currency;

	/**
	 * Create a new Item with the given attributes.
	 *
	 * @param mixed $attributes
	 * @param string|Currency $currency
	 */
	public function __construct($attributes, $currency) {
		$this->currency = $currency;
		parent::__construct($attributes);
	}


	/**
	 * return the price amount for this item
	 * @return Money
	 */
	public function price() {
		return $this->price;
	}

	/**
	 * get the sum of price
	 *
	 * @return Money
	 */
	public function priceSum() {
		return $this->price->multiply($this->quantity);
	}

	/**
	 * get the single price in which conditions are already applied
	 *
	 * @param null|string $type only sum with conditions for this $type
	 * @return Money
	 */
	public function priceWithConditions($type = null) {
		$originalPrice = $this->price->amount();

		$condition_price = $this->conditions->sum(function ($condition) use ($originalPrice, $type) {
			/** @var Condition $condition */
			$price = ($condition && $condition->getTarget() === Condition::TARGET_ITEM
				&& (is_null($type) || $type == $condition->getType()))
				? $condition->applyCondition($originalPrice)
				: 0;
			return $price instanceof Money
				? $price->amount()
				: $price;
		});
		$newPrice = $this->returnPriceAboveZero($condition_price + $originalPrice);
		return new Money((int)$newPrice, $this->currency);
	}

	/**
	 * get the sum of price in which conditions are already applied
	 *
	 * @param null|string $type only sum with conditions for this $type
	 * @return Money
	 * @throws \Bnet\Money\MoneyException
	 */
	public function priceSumWithConditions($type = null) {
		$originalPrice = $this->price->amount();

		$condition_price = $this->conditions
			->filter(function (Condition $cond) use ($type) {
				return $cond->getTarget() === Condition::TARGET_ITEM
					&& (is_null($type) || $cond->getType() == $type);
			})
			->sum(function (Condition $cond) use ($originalPrice) {
				$price = $cond->applyConditionWithQuantity($originalPrice, $this->quantity);
				return $price instanceof Money
					? $price->amount()
					: $price;
			});
		$newPrice = $this->returnPriceAboveZero($condition_price + ($this->quantity * $originalPrice));
		return new Money((int)$newPrice, $this->currency);
	}

	/**
	 * return the price of the conditions without the item price
	 * @param null|string $type only sum with conditions for this $type
	 * @return Money
	 */
	public function priceOnlyConditions($type = null) {
		return $this->priceWithConditions($type)->subtract($this->price());
	}

	/**
	 * return the price of the conditions SUM without the item priceSUM
	 * @param null|string $type only sum with conditions for this $type
	 * @return Money
	 */
	public function priceSumOnlyConditions($type = null) {
		return $this->priceSumWithConditions($type)->subtract($this->priceSum());
	}


	/**
	 * @return int
	 */
	public function taxAmountWithConditions() {
		$tax = 0;
		$item_price = $this->price->amount();

		/* get the tax for the item */
		if ($this->price->hasTax()) {
			/** @var TaxedMoney $taxed_price */
			$taxed_price = $this->price;
			$tax = $taxed_price->taxAmountOnly();
			$item_price = $taxed_price->amountWithoutTax();
		}

		/* summarize all taxes of item conditions that need to be applied as
		 * percent onto the item price */
		$percent_condition_tax_amount = $this->conditions
			->filter(function (CurrencyCondition $cond) {
				return $cond->valueIsPercentage($cond->getValue());
			})
			->sum(function (CurrencyCondition $cond) use ($item_price) {
				if ($this->price->hasTax()) {
					$cond_price = (int)$cond->applyConditionWithQuantity($item_price, $this->quantity)->amount();
					return (new TaxedMoney($cond_price, $this->currency, $this->price->tax))->taxAmountOnly();
				}
				return 0;
			});

		/* summarize all taxes of item conditions that have absolute values */
		$absolute_condition_tax_amount = $this->conditions
			->filter(function (CurrencyCondition $cond) {
				return $cond->getValue() instanceof TaxedMoney;
			})
			->sum(function (CurrencyCondition $cond) use ($item_price) {
				if ($cond->getValue() instanceof TaxedMoney) {
					/* @var TaxedMoney $value */
					$value = $cond->getValue();
					$cond_price = $value->amountWithoutTax() * ($cond->getQuantityIndependent() ? 1 : $this->quantity);
					return (new TaxedMoney($cond_price, $value->currency(), $value->tax))->taxAmountOnly();
				}
				return 0;
			});

		/* add everything up */
		return $this->returnPriceAboveZero(
			$this->quantity * $tax
			+ $percent_condition_tax_amount
			+ $absolute_condition_tax_amount);
	}

}