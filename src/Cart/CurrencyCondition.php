<?php
/**
 * User: thorsten
 * Date: 27.07.16
 * Time: 09:39
 */

namespace Bnet\Cart;


use Bnet\Money\Currency;
use Bnet\Money\Money;
use Bnet\Money\MoneyException;
use Bnet\Money\TaxedMoney;

class CurrencyCondition extends Condition {

	/**
	 * @var Currency
	 */
	protected $currency;

	/**
	 * @var int tax percentage
	 */
	protected $tax;

	/**
	 * CurrencyCondition constructor.
	 * @param array $args
	 * @throws MoneyException
	 * @throws Exceptions\InvalidConditionException
	 */
	public function __construct(array $args) {
		if (isset($args['tax'])) {
			$this->tax = $args['tax'];
			unset($args['tax']);
		}
		/* when the value is a `TaxedMoney` we use its tax and check if there is a mismatch */
		if (isset($args['value']) && $args['value'] instanceof TaxedMoney) {
			if (isset($this->tax) && $this->tax != $args['value']->tax) {
				throw new MoneyException('The tax of the condition and the value do not match.');
			}
			$this->tax = $args['value']->tax;
		}
		// remove the "string" validator from the value field
		$this->rules['value'] = 'required';
		parent::__construct($args);
	}

	/**
	 * apply condition with the given quantity
	 *
	 * @param $totalOrSubTotalOrPrice
	 * @param $conditionValue
	 * @return int
	 */
	protected function applyWithQuantity($totalOrSubTotalOrPrice, $conditionValue, $quantity = 1) {
		return $this->getQuantityIndependent()
			? $this->apply($totalOrSubTotalOrPrice, $conditionValue)
			: $this->apply($totalOrSubTotalOrPrice, $conditionValue) * $quantity;
	}

	/**
	 * apply condition to total or subtotal
	 *
	 * @param Money|int $totalOrSubTotalOrPrice
	 * @return Money
	 */
	public function applyCondition($totalOrSubTotalOrPrice) {
		$totalOrSubTotalOrPrice = $totalOrSubTotalOrPrice instanceof Money
			? $totalOrSubTotalOrPrice->amount()
			: $totalOrSubTotalOrPrice;
		$price = (int)parent::applyCondition($totalOrSubTotalOrPrice);
		return $this->tax
			? new TaxedMoney($price, $this->currency, $this->tax)
			: new Money($price, $this->currency);
	}

	/**
	 * apply condition to total or subtotal
	 *
	 * @param Money|int $totalOrSubTotalOrPrice
	 * @param int $quantity
	 * @return Money
	 */
	public function applyConditionWithQuantity($totalOrSubTotalOrPrice, $quantity) {
		$totalOrSubTotalOrPrice = $totalOrSubTotalOrPrice instanceof Money
			? $totalOrSubTotalOrPrice->amount()
			: $totalOrSubTotalOrPrice;
		$price = (int)parent::applyConditionWithQuantity($totalOrSubTotalOrPrice, $quantity);
		return $this->tax
			? new TaxedMoney($price, $this->currency, $this->tax)
			: new Money($price, $this->currency);
	}

	/**
	 * get the calculated value of this condition supplied by the subtotal|price
	 *
	 * @param Money|int $totalOrSubTotalOrPrice
	 * @return mixed
	 */
	public function getCalculatedValue($totalOrSubTotalOrPrice) {
		$totalOrSubTotalOrPrice = $totalOrSubTotalOrPrice instanceof Money
			? $totalOrSubTotalOrPrice->amount()
			: $totalOrSubTotalOrPrice;
		$price = (int)parent::getCalculatedValue($totalOrSubTotalOrPrice);
		return $this->tax
			? new TaxedMoney($price, $this->currency, $this->tax)
			: new Money($price, $this->currency);
	}

	/**
	 * @param Currency $currency
	 */
	public function setCurrency(Currency $currency) {
		$this->currency = $currency;
	}

	/**
	 * @param int $tax tax percentage
	 */
	public function setTax($tax) {
		$this->tax = $tax;
	}

	/**
	 * removes some arithmetic signs (%,+,-) only
	 *
	 * @param mixed|Money $value
	 * @return mixed
	 */
	protected function cleanValue($value) {
		if ($this->tax && $value instanceof TaxedMoney) {
			return parent::cleanValue($value->amountWithoutTax());
		}
		return parent::cleanValue($value instanceof Money ? $value->amount() : $value);
	}

}