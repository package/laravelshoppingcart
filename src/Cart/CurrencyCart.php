<?php
/**
 * User: thorsten
 * Date: 13.07.16
 * Time: 22:48
 */

namespace Bnet\Cart;
use Bnet\Money\Currency;
use Bnet\Money\Money;
use Bnet\Money\MoneyException;
use Bnet\Money\TaxedMoney;

/**
 * Class CurrencyCart - same as Cart, but work with Money Objects with Currency as pricees
 * @package Bnet\Cart
 */
class CurrencyCart extends Cart {

	/**
	 * @var string Alpha ISo Code of the default currency for this cart
	 */
	protected $currency;

	/**
	 * our object constructor
	 *
	 * @param \Illuminate\Session\SessionManager $session
	 * @param \Illuminate\Contracts\Events\Dispatcher $events
	 * @param string $instanceName
	 * @param string $session_key
	 * @param string|Currency $currency Alpha IsoCode of the Currency of this Cart - only items with this currency are allowed and items without currency get this currency
	 * @param array $custom_item_rules overwrite existing item_rules
	 */
	public function __construct($session, \Illuminate\Contracts\Events\Dispatcher $events, $instanceName, $session_key, $currency='EUR', $custom_item_rules = []) {
		$this->currency = $currency instanceof Currency ? $currency : new Currency($currency);
		parent::__construct($session, $events, $instanceName, $session_key, $custom_item_rules);
	}

    /**
     * get cart sub total - items WITH conditions WITHOUT cart conditions
     *
     * @param null|string $only_with_condition_type Only with this conditionType
     * @return Money
     * @throws MoneyException
     */
	public function subTotal($only_with_condition_type = null) {
		$sum = $this->items()->sum(function (CurrencyItem $item) use ($only_with_condition_type) {
			return $item->priceSumWithConditions($only_with_condition_type)->amount();
		});

		return new Money($sum, $this->currency);
	}

	/**
	 * cart totel WITH cart conditions and WITH item conditions
	 *
	 * @param null|string $only_with_condition_type Only with this conditionType
	 * @return Money
	 * @throws MoneyException
	 */
	public function total($only_with_condition_type = null) {
		$subTotal = $this->subTotal($only_with_condition_type);
		if ($this->getConditions($only_with_condition_type)->isEmpty()) {
			return $subTotal;
		}

		$subTotal = $subTotal->amount();

		/* all cart conditions like fees or shipping are applied */
		$condTotal = $this->getConditions($only_with_condition_type)
			->filter(function (Condition $cond) {
				return $cond->getTarget() === Condition::TARGET_CART;
			})
			->sum(function (Condition $cond) use ($subTotal) {
				$price = $cond->applyCondition($subTotal);
				return $price instanceof Money
					? $price->amount()
					: $price;
			});

		/* all conditions affecting the price are applied */
		$grand_total = $this->getConditions()
			->filter(function (Condition $condition) {
				return $condition->getTarget() === Condition::TARGET_PRICE;
			})->reduce(function ($acc, Condition $cond) {
				return $acc + $cond->applyCondition($acc)->amount();
			}, $subTotal + $condTotal);

		return new Money((int)($grand_total < 0 ? 0 : $grand_total), $this->currency);
	}

	/**
	 * get totalItems WITH conditions WITHOUT item price
	 * @param null|string $type type of conditions to calc only
	 * @return Money
	 * @throws MoneyException
	 */
	public function totalItemsOnlyConditions($type = null) {
		$sum = $this->items()->sum(function (Item $item) use ($type) {
			return $item->priceSumOnlyConditions($type)->amount();
		});

		return new Money($sum, $this->currency);
	}

	/**
	 * cart total WITH cart conditions and WITH item conditions
	 *
	 * @param null|string $type Only with this conditionType
	 * @return Money
	 * @throws MoneyException
	 */
	public function totalOnlyConditions($type = null) {
		$cond = $type
			? $this->getConditionsByType($type)
			: $this->getConditions();
		$itemConditionTotal = $this->totalItemsOnlyConditions($type);
		if ($cond->isEmpty()) {
			return $itemConditionTotal;
		}

		$condTotal = $this->totalOnlyCartConditions($type);

		return $condTotal->add($itemConditionTotal);
	}

	/**
	 * cart total WITH cart conditions WITHOUT item conditions
	 *
	 * @@param null|string $type Only with this conditionType
	 * @return Money
	 * @throws MoneyException
	 */
	public function totalOnlyCartConditions($type = null) {
		$subTotal = $this->subTotal();
		$conditions = $type
			? $this->getConditionsByType($type)
			: $this->getConditions();

		return $conditions
			->filter(function (Condition $cond) {
				return $cond->getTarget() === Condition::TARGET_CART;
			})
			->reduce(function (Money $carry, CurrencyCondition $cond) use ($subTotal) {
				$price = $cond->applyCondition($subTotal);

				/* the amount with tax is needed because we calculate a payment price */
				if ($price instanceof TaxedMoney) {
					$price = $price->amountWithTax();
				}

				// need a money obj to summarize
				if (!$price instanceof Money) {
					$price = new Money((int)$price, $this->currency);
				}

				return $carry->add($price);
			}, new Money(0));
	}

	/**
	 * get cart sub total - items WITHOUT conditions and WITHOUT cart conditions
	 *
	 * @return Money
	 * @throws MoneyException
	 */
	public function totalItemsWithoutConditions() {
		$sum = $this->items()->sum(function (Item $item) {
			if ($item->priceSum() instanceof Money) {
				return $item->priceSum()->amount();
			}
			return $item->priceSum();
		});

		return new Money($sum, $this->currency);
	}

	/**
	 * get cart sub total - items WITH conditions and WITHOUT cart conditions
	 * alias for subTotal
	 * @param null|string $only_with_condition_type Only with this conditionType
	 * @return Money
	 */
	public function totalItems($only_with_condition_type = null) {
		return $this->subTotal($only_with_condition_type);
	}

	/**
	 * return the total tax amount
	 * @return Money
	 */
	public function totalTaxAmount() {
		$sum = $this->items()->sum(function(CurrencyItem $item) {
			return $item->taxAmountWithConditions();
		});
		return new Money($sum, $this->currency);
	}

	/**
	 * get the cart
	 *
	 * @return CurrencyItems
	 */
	public function items() {
		return (new CurrencyItems($this->session->get($this->sessionKeyCartItems)));
	}

	/**
	 * get a collection of price conditions and their actual value
	 * @return Conditions
	 * @throws MoneyException
	 */
	public function getPriceConditions() {
		$subtotal_with_conditions = $this->subTotal()->add($this->totalOnlyCartConditions());
		return $this->getConditions()
			->filter(function (Condition $c) {
				return $c->getTarget() === Condition::TARGET_PRICE;
			})->map(function (CurrencyCondition $cond) use (&$subtotal_with_conditions) {
				$cond_value = $cond->applyCondition($subtotal_with_conditions);
				$cond_clone = clone $cond;
				/* when a negative condition results in a negative total
				 * only the remainder of the condition is used */
				if (($diff = $subtotal_with_conditions->add($cond_value))->amount() < 0) {
					$subtotal_with_conditions = new Money(0);
					/* only the remainder is used for the condition value */
					$cond_clone['value'] = $cond_value->subtract($diff);
				} else {
					$subtotal_with_conditions = $subtotal_with_conditions->add($cond_value);
					$cond_clone['value'] = $cond_value;
				}
				return $cond_clone;
			});
	}

	/**
	 * add row to cart collection
	 *
	 * @param $id
	 * @param $item
	 */
	protected function addRow($id, $item) {
		if (!$item['price'] instanceof Money)
			$item['price'] = new Money($item['price'], @$item['currency'] ?: $this->currency);
		parent::addRow($id, $item);
	}

	/**
	 * add item to the cart, it can be an array or multi dimensional array
	 *
	 * @param string|array $id
	 * @param string $name
	 * @param Money $price
	 * @param int $quantity
	 * @param array $attributes
	 * @param Condition|array $conditions
	 * @return $this
	 * @throws \Bnet\Cart\Exceptions\InvalidItemException
	 */
	public function add($id, $name = null, $price = null, $quantity = 1, $attributes = array(), $conditions = array()) {
		if (!is_null($price) && !$price->currency()->equals($this->currency))
			throw new \Bnet\Cart\Exceptions\CurrencyNotMachedException('given item-currency ['.$price->currency()->code.'] does not match to cart currency ['.$this->currency->code.']' );
		return parent::add($id, $name, $price, $quantity, $attributes, $conditions);
	}


	/**
	 * create the object for an cart item
	 * @param $data
	 * @return Item
	 */
	protected function createCartItem($data) {
		return new CurrencyItem($data, $this->currency);
	}


	/**
	 * get an item on a cart by item ID
	 *
	 * @param $itemId
	 * @return CurrencyItem
	 */
	public function get($itemId) {
		$item = parent::get($itemId);
		if (!$item['price'] instanceof Money)
			$item['price'] = new Money($item['price'], @$item['currency'] ?: $this->currency);
		return $item;
	}

	/**
	 * add condition on an existing item on the cart
	 *
	 * @param int|string $productId
	 * @param Condition $itemCondition
	 * @return CurrencyCart
	 */
	public function addItemCondition($productId, Condition $itemCondition) {
		// add the cart currency to the condition
		if ($itemCondition instanceof CurrencyCondition)
			$itemCondition->setCurrency($this->currency);
		return parent::addItemCondition($productId, $itemCondition);
	}

}